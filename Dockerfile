FROM tomcat

RUN curl -sL https://deb.nodesource.com/setup_9.x | bash -
RUN apt-get update
RUN apt-get -y install nodejs
RUN apt-get -y install maven

ENV NO_UPDATE_NOTIFIER=true
ENV IMDB24_REST_API=http://localhost:8089/imdb24
RUN mkdir /usr/local/server
RUN mkdir /usr/local/client

WORKDIR /usr/local/server
COPY server/ ./
RUN mvn clean package
RUN mv target/imdb24-1.0.0.war /usr/local/tomcat/webapps/imdb24.war

WORKDIR /usr/local/client
COPY client/ ./
RUN npm install --silent
RUN npm run build
RUN rm -r /usr/local/tomcat/webapps/ROOT/
RUN mkdir /usr/local/tomcat/webapps/ROOT/
RUN mkdir /usr/local/tomcat/webapps/films/
RUN cp -r target/* /usr/local/tomcat/webapps/ROOT/
RUN cp -r target/* /usr/local/tomcat/webapps/films/

EXPOSE 8080

WORKDIR /usr/local/tomcat
CMD ["catalina.sh", "run"]
