import React from 'react';

export default {
  login: "Login",
  password: "Password",
  register: "Register",
  logout: "Logout",
  open_films: "Go to films",
  previous: "Previous",
  next: "Next",
  avgRating: "Average rating is",
  userRating: "Your rating is",
  search: "Search for some film",
  title: "Imdb24",
}
