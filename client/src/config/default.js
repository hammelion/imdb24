export default {
  endpoints: {
    core_api: {
      login: `${IMDB24_REST_API}/auth/login`,
      register: `${IMDB24_REST_API}/auth/register`,
      currentuser: `${IMDB24_REST_API}/rest/currentuser`,
      films: `${IMDB24_REST_API}/rest/films`,
      ratings: `${IMDB24_REST_API}/rest/ratings`
    }
  },
  page_size: 8
};

