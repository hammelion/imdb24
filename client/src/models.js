import rest from 'utils/rest';
import Cookies from 'js-cookie'

export const logged_in = {
  state: Cookies.get("__SessionId") !== undefined,
  reducers: {
    yes: () => true,
    no: () => false,
  },
}

export const currentuser = {
  state: null,
  reducers: {
    set: (s, p) => p,
    unset: () => null
  },
  effects: (dispatch) => ({
    async load(p, s) {
      await rest.currentuser();
    }
  })
}

export const films = {
  state: null,
  reducers: {
    set: (s, p) => p.films,
  },
  effects: (dispatch) => ({
    async load(p, s) {
      await rest.films(p);
    },
    async rate(p, s) {
      await rest.rate(p);
    }
  })
}

export const film_search = {
  state: "",
  reducers: {
    set: (s, p) => p
  }
}
