import './.scss';
import React from 'react';
import imgLogo from 'images/logo.jpg';
import Paper from '@material-ui/core/Paper';
import OpenApp from 'sections/open-app'
import {useRematchState} from 'store';

export default () => {
  const [i18n] = useRematchState("i18n");

  return <div className="landing-cmp">
    <Paper elevation={11} className="landing-top" style={{backgroundImage: `url(${imgLogo}`}}>
      <div>
        <h1>{i18n.title}</h1>
        <div className="landing-to-action-container">
          <Paper elevation={10}>
            <OpenApp/>
          </Paper>
        </div>
      </div>
    </Paper>
  </div>
}
