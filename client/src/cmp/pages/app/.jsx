import './.scss';
import React, {useEffect} from 'react';
import Paper from '@material-ui/core/Paper';
import FilmsSection from 'sections/films'
import {useRematchState} from 'store';

export default ({create}) => {
  const [i18n] = useRematchState("i18n");
  const [currentuser, dspCurrentuser] = useRematchState("currentuser");
  const [films, dspFilms] = useRematchState("films");

  useEffect(() => {
    if (currentuser === null) {
      dspCurrentuser.load();
    }
    if (films === null) {
      dspFilms.load();
    }
  }, [])

  return (
    <div className="app-cmp">
      <Paper elevation={3}>
        <div className="container">
          <div className="section">
            <FilmsSection create={create}/>
          </div>
        </div>
      </Paper>
    </div>
  )
}
