import './.scss';
import React, {useState} from 'react';
import {useRematchState} from 'store';
import OrangeButton from 'buttons/orange-button';
import CardActions from "@material-ui/core/CardActions";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import StarRatingComponent from 'react-star-rating-component';
import InputBase from "@material-ui/core/InputBase";
import Paper from "@material-ui/core/Paper";

export default () => {
  const [i18n] = useRematchState("i18n");
  const [config] = useRematchState("config");
  const pageSize = config.page_size;
  const [films, dspFilms] = useRematchState("films");
  const [currentuser] = useRematchState("currentuser");
  const [film_search, dspFilmSearch] = useRematchState("film_search");
  const [currentPage, setCurrentPage] = useState(0);

  const previous = () => {
    dspFilms.load({name: film_search, page: currentPage - 1, size: pageSize})
    setCurrentPage(currentPage - 1);
  }

  const next = () => {
    dspFilms.load({name: film_search, page: currentPage + 1, size: pageSize})
    setCurrentPage(currentPage + 1);
  }

  const rated = (filmId) => {
    const rating = currentuser.ratedFilms.find(r => r.filmId === filmId);
    return rating ? rating.rating : undefined;
  }

  const search = (name) => {
    setCurrentPage(0);
    dspFilmSearch.set(name);
    dspFilms.load({page: 0, name});
  }

  const rate = (film, rating) => {
    dspFilms.rate({
      filmId: film.id,
      rating,
      filmsData: {name: film_search, page: currentPage, size: pageSize}
    })
  }

  const Film = ({film}) => {
    return <Grid item xs={6}>
      <Card className="film-card">
        <CardContent>
          <Typography variant="h5" component="h2">
            {film.name}
          </Typography>
          <Typography color="textSecondary">
            {`${i18n.avgRating}: ${film.avgRating}`}
          </Typography>
          <StarRatingComponent
            name="rating"
            onStarClick={rating => rate(film, rating)}
            value={rated(film.id)}
          />
        </CardContent>
      </Card>
    </Grid>;
  }

  return (
    films !== null && currentuser !== null &&
    <Card className="film-list-cmp">
      <CardContent>
        <Paper className="search-container">
          <InputBase
            placeholder={i18n.search}
            inputProps={{'aria-label': 'Search'}}
            className="search"
            onChange={e => search(e.target.value)}
          />
        </Paper>
        <Grid container spacing={4}>
          {films.map(film => <Film film={film} key={film.id}/>)}
        </Grid>
      </CardContent>
      <CardActions className="list-navigation">
        {currentPage > 0 &&
        <OrangeButton onClick={e => previous()}>
          {i18n.previous}
        </OrangeButton>}
        {(currentPage > 0 || films.length === pageSize) && <div>{currentPage}</div>}
        {films.length === pageSize && <OrangeButton onClick={e => next()}>
          {i18n.next}
        </OrangeButton>}
      </CardActions>
    </Card>
  );
}
