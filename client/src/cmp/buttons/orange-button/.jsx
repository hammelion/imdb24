import './.scss';
import React from 'react';
import Button from "@material-ui/core/Button";

export default ({children, onClick = e => e}) => {
  return (
    <Button
      className="orange-button"
      variant="contained"
      onClick={onClick}>
      {children}
    </Button>
  )
}
