import i18n from '../i18n/en.jsx';
import config from '../config/default';

import {init} from '@rematch/core'
import * as models from '../models'
import {useEffect, useState} from "react";

const store = init({
  models: {...models, i18n: {state: i18n}, config: {state: config}}
});

export default store;

export const {dispatch, getState} = store;

export const useRematchState = (param) => {
  const [state, setState] = useState(getState()[param]);
  useEffect(() => {
    return store.subscribe(() => {
      setState(getState()[param])
    })
  })

  return [state, dispatch[param]];
}

