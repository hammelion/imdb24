import './.scss';
import React from 'react';
import FilmList from 'lists/film-list';

export default () => {
  return (
    <div className="films-section-cmp">
      <FilmList/>
    </div>
  )
}
