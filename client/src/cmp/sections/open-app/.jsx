import Cookies from 'js-cookie'
import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import OrangeButton from 'buttons/orange-button';
import rest from 'utils/rest'
import {useRematchState} from 'store';

const LoggedIn = ({showLogout = false}) => {
  const [i18n] = useRematchState("i18n");
  const [logged_in, dspLogged_in] = useRematchState("logged_in");
  const [currentuser, dspCurrentUser] = useRematchState("currentuser");

  return (
    <div>
      <Link to="/films">
        <OrangeButton>
          {i18n.open_films}
        </OrangeButton>
      </Link>
      {showLogout
      && <span>
          <br/>
          <Button onClick={() => {
            Cookies.remove('__SessionId');
            dspLogged_in.no();
            dspCurrentUser.unset();
          }}>
            {i18n.logout}
          </Button>
        </span>
      }
    </div>
  )
}

const NotLoggedIn = () => {
  const [i18n] = useRematchState("i18n");
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [errorMsg, setErrorMsg] = useState("");

  return (
    <div>
      <div>
        <TextField label={i18n.login} onChange={(e) => setLogin(e.target.value)}/>
        <br/>
        <br/>
        <TextField label={i18n.password} type="password" onChange={(e) => setPassword(e.target.value)}/><br/>
        <br/>
      </div>
      <OrangeButton onClick={() => rest.login(login, password, e => setErrorMsg(e.response.data.error))}>
        {i18n.login}
      </OrangeButton>
      <OrangeButton onClick={() => rest.register(login, password, e => setErrorMsg(e.response.data.error))}>
        {i18n.register}
      </OrangeButton>
      {errorMsg.split('\n').map((item, i) => <p key={i}>{item}</p>)}
    </div>
  )
}

export default () => {
  const [logged_in] = useRematchState("logged_in");

  return logged_in ? <LoggedIn showLogout/> : <NotLoggedIn/>;
}
