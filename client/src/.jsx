import React from 'react'
import './.scss';
import {Provider} from 'react-redux'
import {Route, Router, Switch} from 'react-router-dom';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import history from 'cmp/hst';
import DocumentTitle from 'react-document-title';
import Landing from 'pages/landing'
import App from 'pages/app'

const theme = createMuiTheme({
  overrides: {
    MuiButton: {
      contained: {
        color: "rgba(0, 0, 0, 0.54)",
      }
    },
    MuiCard: {
      root: {
        overflow: undefined,
      },
    }
  },
  palette: {
    primary: {
      main: "#FF8A65"
    },
    secondary: {
      main: '#ffb74d',
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  },
  typography: {
    htmlFontSize: 15,
    htmlFontWeight: 300,
    fontSize: 14,
    fontWeight: 300,
    fontFamily: [
      'Roboto',
      'sans-serif'
    ].join(', '),
    button: {
      color: "rgba(0, 0, 0, 0.54)",
    },
    h5: {
      fontSize: 18,
    },
    useNextVariants: true
  }
});

export default (props) => {
  return <Provider store={props.store}>
    <MuiThemeProvider theme={theme}>
      <Router history={history}>
        <div>
          <Switch>
            <Route path="/films"
                   component={() => <DocumentTitle title='Imdb24 - Films'>
                     <div><App/></div>
                   </DocumentTitle>}/>
            <Route path="/" component={() => <DocumentTitle
              title='Imdb24'><Landing/></DocumentTitle>}/>
          </Switch>
        </div>
      </Router>
    </MuiThemeProvider>
  </Provider>
};
