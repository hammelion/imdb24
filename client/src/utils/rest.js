import config from '../config/default';
import axios from 'axios';
import history from 'hst';
import {dispatch} from 'store';

const checkAuth = (e) => {
  if (e.response && e.response.status === 401) {
    dispatch.logged_in.no();
    dispatch.currentuser.unset();
    history.push("/");
  }
  console.error(e);
}

axios.defaults.withCredentials = true;

const login = (login, password, fail = e => e) => {
  axios({
    method: 'post',
    url: config.endpoints.core_api.login,
    timeout: 2000,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: `username=${login}&password=${password}`
  }).then(r => {
    dispatch.logged_in.yes();
    history.push("/films");
  }).catch((e) => {
    console.error(e);
    fail(e);
  });
};

const register = (username, password, fail = e => e) => {
  axios({
    method: 'post',
    url: config.endpoints.core_api.register,
    timeout: 2000,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    data: `username=${username}&password=${password}`
  }).then(r => {
    login(username, password, fail);
  }).catch((e) => {
    console.error(e);
    fail(e);
  });
};

const films = ({name = "", page = 0, size = config.page_size} = {}) => {
  return axios({
    method: 'get',
    url: `${config.endpoints.core_api.films}?name=${name}&page=${page}&size=${size}`,
    timeout: 2000
  }).then(r => {
    dispatch.films.set(r.data);
  }).catch(e => checkAuth(e));
};

const currentuser = () => {
  return axios.get(config.endpoints.core_api.currentuser)
    .then(r => {
      dispatch.currentuser.set(r.data);
    }).catch(e => checkAuth(e));
};

const rate = ({filmId, rating, filmsData}) => {
  return axios({
    method: 'post',
    url: `${config.endpoints.core_api.ratings}?film_id=${filmId}&rating=${rating}`,
    timeout: 2000,
  }).then(r => {
    currentuser();
    films(filmsData)
  }).catch(e => checkAuth(e));
};

export default {
  login,
  register,
  currentuser,
  films,
  rate
}
