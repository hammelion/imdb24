import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {AppContainer} from 'react-hot-loader';
import 'react-event-listener';
import App from './.jsx';
import config from 'config/default';

import store from 'cmp/store';

console.info(`Connected to Imdb24 REST API on ${config.endpoints.core_api.base}`);

const render = (Component) => {
  ReactDOM.render(
    <AppContainer>
      <Component store={store}/>
    </AppContainer>,
    document.getElementById('root')
  );
};

render(App);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./.jsx', () => {
    render(App)
  });
}
