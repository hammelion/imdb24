const config = require('./prod.config.js');
const webpack = require('webpack');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

module.exports = Object.assign({}, config, {
  entry: ['react-hot-loader/patch',
    ...config.entry],
  devtool: '#inline-source-map',
  devServer: {
    hot: true,
    inline: true,
    contentBase: config.output.path,
    historyApiFallback: {
      index: "/"
    },
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },
  plugins: [...config.plugins.filter(p => !(p instanceof UglifyJSPlugin)),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()]
});
