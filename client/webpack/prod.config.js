const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

const projectRootPath = path.resolve(__dirname, '../');
const assetsPath = path.resolve(projectRootPath, 'target');


const genVars = (vars) => {
  let obj = {};
  vars.forEach(v => obj[v] = `"${process.env[v] ? process.env[v] : process.env['npm_package_config_' + v]}"`);
  return obj;
}

module.exports = {
  context: path.resolve(__dirname, '..'),
  entry: [
    './src/index.jsx'
  ],
  resolve: {
    modules: [
      path.resolve('./node_modules'),
      path.resolve('./node_modules/stream-browserify/node_modules'),
      path.resolve('./src'),
      path.resolve('./src/cmp'),
    ],
    alias: {
      scss: path.resolve('./src/css/globals.scss'),
      images: path.resolve('./src/cmp/images')
    },
    mainFiles: [
      "index", ".jsx"
    ],
    extensions: [".js", ".json", ".jsx"],
  },
  output: {
    path: assetsPath,
    filename: 'bundle.js',
    publicPath: '/'
  },
  module: {
    rules: [
      {test: /\.jsx$/, exclude: /node_modules/, loader: "babel-loader"},
      {
        test: /\.scss$/,
        use: [{
          loader: "style-loader"
        }, {
          loader: "css-loader", options: {
            sourceMap: true
          }
        }, {
          loader: "sass-loader", options: {
            sourceMap: true
          }
        }]
      },
      {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
        loader: "file-loader",
        options: {
          name: 'static/[hash].[ext]'
        }
      }
    ]
  },
  optimization: {
    minimizer: [new UglifyJSPlugin()]
  },
  plugins: [
    new CopyWebpackPlugin([{from: 'src/index.html', to: 'index.html'}]),
    new DefinePlugin(genVars([
      "IMDB24_REST_API"
    ]))
  ]
};

