if [ $(id -u) != "0" ]
    then
        sudo "$0" "$@"
        exit $?
    fi
image=$(docker build . | tee /dev/tty | tail -n 1 | cut -f 3 -d " ")
docker tag $image ch/imdb24:latest
docker run -i -d -t -p 8089:8080 -p 3030:3030 ch/imdb24:latest 
echo "http://localhost:8089/"
echo "user:bilbo | password:12345678"
