DELETE FROM rating;
DELETE FROM user;
DELETE FROM film;

INSERT INTO user (username, password) VALUES ('bilbo', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f');
INSERT INTO user (username, password) VALUES ('indiana', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f');
INSERT INTO user (username, password) VALUES ('james', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f');
INSERT INTO user (username, password) VALUES ('han', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f');
INSERT INTO user (username, password) VALUES ('tyler', 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f');

INSERT INTO film (id, name) VALUES (1, 'Lord of the Rings');
INSERT INTO rating (user, film, rating) VALUES ('bilbo', 1, 1);
INSERT INTO rating (user, film, rating) VALUES ('indiana', 1, 2);
INSERT INTO rating (user, film, rating) VALUES ('james', 1, 3);
INSERT INTO rating (user, film, rating) VALUES ('han', 1, 4);
INSERT INTO rating (user, film, rating) VALUES ('tyler', 1, 5);

INSERT INTO film (id, name) VALUES (2, 'Lord of the Flies');
INSERT INTO rating (user, film, rating) VALUES ('bilbo', 2, 5);
INSERT INTO rating (user, film, rating) VALUES ('indiana', 2, 5);
INSERT INTO rating (user, film, rating) VALUES ('james', 2, 5);
INSERT INTO rating (user, film, rating) VALUES ('han', 2, 5);
INSERT INTO rating (user, film, rating) VALUES ('tyler', 2, 5);

INSERT INTO film (id, name) VALUES (3, 'Teenagers from Outer Space');



