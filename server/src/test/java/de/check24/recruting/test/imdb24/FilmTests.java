package de.check24.recruting.test.imdb24;

import de.check24.recruting.test.imdb24.io.in.rest.jersey.FilmsAdapter;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.responses.FilmSearchResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static de.check24.recruting.test.imdb24.AuthTests.login;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpMethod.GET;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FilmTests {
    public static final String FILM_URL = "/rest/films";
    @Inject
    private TestRestTemplate rest;

    public static ResponseEntity<FilmSearchResponse> getFilm(TestRestTemplate rest, String name) {
        URI url = UriBuilder.fromUri(FILM_URL)
                .queryParam(FilmsAdapter.P_NAME, name)
                .build();
        return rest.exchange(url, GET, login(rest), FilmSearchResponse.class);
    }

    @Test
    public void get_films_success() throws Exception {
        ResponseEntity<FilmSearchResponse> test = rest.exchange(FILM_URL, GET, login(rest), FilmSearchResponse.class);
        assertEquals(200, test.getStatusCodeValue());
        assertEquals("Wrong result", 3, test.getBody().films.size());
    }

    @Test
    public void get_films_by_name_success() throws Exception {
        ResponseEntity<FilmSearchResponse> test = getFilm(rest, "Lord of");
        assertEquals(200, test.getStatusCodeValue());
        assertEquals("Wrong result", 2, test.getBody().films.size());
    }

    @Test
    public void get_films_by_name_page_0_success() throws Exception {
        URI url = UriBuilder.fromUri(FILM_URL)
                .queryParam(FilmsAdapter.P_NAME, "Lord of")
                .queryParam(FilmsAdapter.P_PAGE, 0)
                .queryParam(FilmsAdapter.P_SIZE, 1)
                .build();
        ResponseEntity<FilmSearchResponse> test = rest.exchange(url, GET, login(rest), FilmSearchResponse.class);
        assertEquals(200, test.getStatusCodeValue());
        assertEquals("Wrong result", 1, test.getBody().films.size());
    }

    @Test
    public void get_films_by_name_page_1_success() throws Exception {
        URI url = UriBuilder.fromUri(FILM_URL)
                .queryParam(FilmsAdapter.P_NAME, "Lord of")
                .queryParam(FilmsAdapter.P_PAGE, 1)
                .queryParam(FilmsAdapter.P_SIZE, 1)
                .build();
        ResponseEntity<FilmSearchResponse> test = rest.exchange(url, GET, login(rest), FilmSearchResponse.class);
        assertEquals(200, test.getStatusCodeValue());
        assertEquals("Wrong result", 1, test.getBody().films.size());
    }
}
