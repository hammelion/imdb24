package de.check24.recruting.test.imdb24;

import de.check24.recruting.test.imdb24.io.in.rest.jersey.responses.FilmSearchResponsePart;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.responses.UserResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

import static de.check24.recruting.test.imdb24.AuthTests.login;
import static de.check24.recruting.test.imdb24.io.in.rest.jersey.RatingsAdapter.P_FILM;
import static de.check24.recruting.test.imdb24.io.in.rest.jersey.RatingsAdapter.P_RATING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.HttpMethod.POST;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RatingTests {
    public static final String RATING_URL = "/rest/ratings";
    public static final String FILM_ID = "3";
    public static final String FILM_NAME = "Teenagers from Outer Space";
    @Inject
    private TestRestTemplate rest;

    @Test
    public void rate_missing_film_id() throws Exception {
        ResponseEntity<String> test = rest.exchange(RATING_URL, POST, login(rest), String.class);
        assertEquals(400, test.getStatusCodeValue());
    }

    @Test
    public void rate_missing_rating() throws Exception {
        URI url = UriBuilder.fromUri(RATING_URL)
                .queryParam(P_FILM, FILM_ID)
                .build();
        ResponseEntity<String> test = rest.exchange(url, POST, login(rest), String.class);
        assertEquals(400, test.getStatusCodeValue());
    }

    @Test
    public void rate_invalid_rating() throws Exception {
        URI url = UriBuilder.fromUri(RATING_URL)
                .queryParam(P_FILM, FILM_ID)
                .queryParam(P_RATING, 6)
                .build();
        ResponseEntity<String> test = rest.exchange(url, POST, login(rest), String.class);
        assertEquals(400, test.getStatusCodeValue());
    }

    @Test
    public void rate_success() throws Exception {
        final Integer NEW_RATING = 5;
        URI url = UriBuilder.fromUri(RATING_URL)
                .queryParam(P_FILM, FILM_ID)
                .queryParam(P_RATING, NEW_RATING)
                .build();
        ResponseEntity<String> test = rest.exchange(url, POST, login(rest), String.class);
        assertEquals(201, test.getStatusCodeValue());

        final FilmSearchResponsePart film = FilmTests.getFilm(rest, FILM_NAME)
                .getBody().films.get(0);
        assertEquals("Film rating not updated", NEW_RATING, film.avgRating);

        final UserResponse user = UserTests.getUserBilbo(rest);
        assertTrue("User rating not updated", user.ratedFilms.stream()
                .anyMatch(f -> f.filmId.equals(Long.valueOf(FILM_ID)) && f.rating.equals(NEW_RATING)));
    }
}
