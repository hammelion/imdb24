package de.check24.recruting.test.imdb24;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Optional;

import static javax.ws.rs.core.HttpHeaders.SET_COOKIE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.http.HttpMethod.GET;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AuthTests {
    public static final String EXISTING_USERNAME = "bilbo";
    public static final String NEW_USERNAME = "frodo";
    public static final String WRONG_USERNAME = "sam";
    public static final String PASSWORD = "12345678";
    public static final String MISSING_ERROR_INFORMATION = "Missing error information";
    public static final String LOGIN_URL = "/auth/login";
    public static final String REGISTER_URL = "/auth/register";
    @Inject
    private TestRestTemplate rest;

    private static HttpEntity loginRequest(final String username, final String password) {
        LinkedMultiValueMap headers = new LinkedMultiValueMap();
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED);

        LinkedMultiValueMap map = new LinkedMultiValueMap();
        map.add("username", username);
        map.add("password", password);

        return new HttpEntity(map, headers);
    }


    private static boolean has(ResponseEntity<String> responseEntity, String text) {
        return responseEntity.getBody() != null && responseEntity.getBody()
                .contains(text);
    }

    public static HttpEntity login(TestRestTemplate rest) {
        ResponseEntity<String> responseEntity = rest.postForEntity(LOGIN_URL, loginRequest(EXISTING_USERNAME, PASSWORD), String.class);
        assertEquals(202, responseEntity.getStatusCodeValue());
        Optional<String> sessionId = Optional.ofNullable(responseEntity.getHeaders()
                .get(SET_COOKIE))
                .map(c -> c.get(0));
        assertTrue("No session id in cookies", sessionId.isPresent());

        LinkedMultiValueMap headers = new LinkedMultiValueMap();
        headers.add(HttpHeaders.COOKIE, sessionId.get());
        return new HttpEntity(headers);
    }

    @PostConstruct
    public void setUp() throws Exception {
        rest.getRestTemplate()
                .setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        rest.getRestTemplate()
                .setErrorHandler(new DefaultResponseErrorHandler() {
                    public boolean hasError(ClientHttpResponse response) throws IOException {
                        HttpStatus statusCode = response.getStatusCode();
                        return statusCode.series() == HttpStatus.Series.SERVER_ERROR;
                    }
                });
    }

    @Test
    public void login_failed_invalid_username() throws Exception {
        ResponseEntity<String> responseEntity = rest.postForEntity(LOGIN_URL, loginRequest(null, null), String.class);
        assertEquals(401, responseEntity.getStatusCodeValue());
        assertTrue(MISSING_ERROR_INFORMATION, has(responseEntity, "username"));
    }

    @Test
    public void login_failed_invalid_password() throws Exception {
        ResponseEntity<String> responseEntity = rest.postForEntity(LOGIN_URL, loginRequest(EXISTING_USERNAME, "1234"), String.class);
        assertEquals(401, responseEntity.getStatusCodeValue());
        assertTrue(MISSING_ERROR_INFORMATION, has(responseEntity, "password"));
    }

    @Test
    public void login_failed_nonexisting_user() throws Exception {
        ResponseEntity<String> responseEntity = rest.postForEntity(LOGIN_URL, loginRequest(WRONG_USERNAME, PASSWORD), String.class);
        assertEquals(401, responseEntity.getStatusCodeValue());
        assertTrue(MISSING_ERROR_INFORMATION, has(responseEntity, "wrong credentials"));

    }

    @Test
    public void login_failed_wrong_password() throws Exception {
        ResponseEntity<String> responseEntity = rest.postForEntity(LOGIN_URL, loginRequest(EXISTING_USERNAME, "12341234"), String.class);
        assertEquals(401, responseEntity.getStatusCodeValue());
        assertTrue(MISSING_ERROR_INFORMATION, has(responseEntity, "wrong credentials"));

    }

    @Test
    public void login_success() throws Exception {
        ResponseEntity<String> test = rest.exchange("/rest/currentuser", GET, login(rest), String.class);
        assertEquals(200, test.getStatusCodeValue());
    }

    @Test
    public void register_existing_user() throws Exception {
        ResponseEntity<String> badResponse = rest.postForEntity(REGISTER_URL, loginRequest(EXISTING_USERNAME, PASSWORD), String.class);
        assertEquals(400, badResponse.getStatusCodeValue());
    }

    @Test
    public void register_new_user() throws Exception {
        ResponseEntity<String> failedLogin = rest.postForEntity(LOGIN_URL, loginRequest(NEW_USERNAME, PASSWORD), String.class);
        assertEquals(401, failedLogin.getStatusCodeValue());

        ResponseEntity<String> goodResponse = rest.postForEntity(REGISTER_URL, loginRequest(NEW_USERNAME, PASSWORD), String.class);
        assertEquals(201, goodResponse.getStatusCodeValue());

        ResponseEntity<String> successLogin = rest.postForEntity(LOGIN_URL, loginRequest(NEW_USERNAME, PASSWORD), String.class);
        assertEquals(202, successLogin.getStatusCodeValue());
    }

}
