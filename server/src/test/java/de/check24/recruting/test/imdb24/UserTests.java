package de.check24.recruting.test.imdb24;

import de.check24.recruting.test.imdb24.io.in.rest.jersey.responses.UserResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.inject.Inject;

import static de.check24.recruting.test.imdb24.AuthTests.login;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpMethod.GET;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserTests {
    public static final String USER_URL = "/rest/currentuser";
    @Inject
    private TestRestTemplate rest;

    public static UserResponse getUserBilbo(TestRestTemplate rest) {
        ResponseEntity<UserResponse> test = rest.exchange(USER_URL, GET, login(rest), UserResponse.class);
        assertEquals(200, test.getStatusCodeValue());
        return test.getBody();
    }

    @Test
    public void get_user_success() throws Exception {
        getUserBilbo(rest);
    }

}
