package de.check24.recruting.test.imdb24.io.out.domain_storage;

import de.check24.recruting.test.imdb24.core.domain.*;
import de.check24.recruting.test.imdb24.core.domain.validated.AddRating;
import de.check24.recruting.test.imdb24.core.domain.validated.LoginData;
import de.check24.recruting.test.imdb24.core.domain.validated.Username;
import io.vavr.control.Option;

import java.util.List;

/**
 * Required domain model: Film, User
 */
public interface DomainStoragePort {
    Option<User> authenticateUser(final LoginData loginData);

    Option<User> createUser(final LoginData loginData);

    Option<User> getUser(final Username username);

    List<Film> searchFilms(final FilmSearch filmSearch);

    Option<Long> addRating(final AddRating addRating);
}
