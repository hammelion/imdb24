package de.check24.recruting.test.imdb24.core.domain.failures;

public class AuthFailure {
    public final String reason;
    public final boolean technical;

    public AuthFailure(final String reason, final boolean technical) {
        this.reason = reason;
        this.technical = technical;
    }

    @Override
    public String toString() {
        return "AuthFailure{" +
                "reason='" + reason + '\'' +
                '}';
    }
}
