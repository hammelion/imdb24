package de.check24.recruting.test.imdb24.core;

import de.check24.recruting.test.imdb24.core.domain.failures.AuthFailure;
import de.check24.recruting.test.imdb24.core.domain.validated.LoginData;
import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import de.check24.recruting.test.imdb24.io.out.domain_storage.DomainStoragePort;
import de.check24.recruting.test.imdb24.io.out.session_storage.SessionStoragePort;
import de.check24.recruting.test.imdb24.core.domain.*;
import io.vavr.control.Either;
import io.vavr.control.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class SecurityInteractor {
    public static final AuthFailure REGISTER_FAIL = new AuthFailure("could not register a new user", false);
    private static Logger LOG = LoggerFactory.getLogger(SecurityInteractor.class);

    private final SessionStoragePort sessionStoragePort;
    private final DomainStoragePort domainStoragePort;

    @Inject
    public SecurityInteractor(final SessionStoragePort sessionStoragePort, final DomainStoragePort domainStoragePort) {
        this.sessionStoragePort = sessionStoragePort;
        this.domainStoragePort = domainStoragePort;
    }

    public Either<AuthFailure, SessionId> login(final LoginData loginData) {
        return domainStoragePort.authenticateUser(loginData)
                .toEither(new AuthFailure("wrong credentials", false))
                .flatMap(user -> sessionStoragePort.createSession(user)
                        .mapLeft(f -> {
                            LOG.error(f.e.getMessage(), f.e);
                            return new AuthFailure("technical problem", true);
                        }));
    }

    public Either<AuthFailure, User> register(final LoginData loginData) {
        Option<User> existingUser = domainStoragePort.authenticateUser(loginData);
        if (!existingUser.isEmpty()) {
            return Either.left(REGISTER_FAIL);
        }

        return domainStoragePort.createUser(loginData)
                .toEither(REGISTER_FAIL);
    }

    public Either<AuthFailure, Session> authenticate(final SessionId sessionId) {
        return sessionStoragePort.getSession(sessionId)
                .mapLeft(f -> {
                    LOG.error(f.e.getMessage(), f.e);
                    return new AuthFailure("technical problem", true);
                });
    }
}
