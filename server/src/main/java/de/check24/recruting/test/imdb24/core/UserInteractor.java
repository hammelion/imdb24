package de.check24.recruting.test.imdb24.core;

import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import de.check24.recruting.test.imdb24.core.domain.User;
import de.check24.recruting.test.imdb24.core.domain.validated.Username;
import de.check24.recruting.test.imdb24.core.domain.failures.UserFailure;
import de.check24.recruting.test.imdb24.io.out.domain_storage.DomainStoragePort;
import de.check24.recruting.test.imdb24.io.out.session_storage.SessionStoragePort;
import io.vavr.control.Either;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class UserInteractor {
    private static Logger LOG = LoggerFactory.getLogger(UserInteractor.class);

    private final SessionStoragePort sessionStoragePort;
    private final DomainStoragePort domainStoragePort;

    @Inject
    public UserInteractor(final SessionStoragePort sessionStoragePort, final DomainStoragePort domainStoragePort) {
        this.sessionStoragePort = sessionStoragePort;
        this.domainStoragePort = domainStoragePort;
    }

    public Either<UserFailure, User> getCurrentUser(final SessionId sessionId) {
        return sessionStoragePort.getSession(sessionId)
                .mapLeft(f -> new UserFailure(f.e.getMessage()))
                .flatMap(session -> Username.of(session.username)
                        .mapLeft(f -> new UserFailure(f.errors())))
                .flatMap(username -> domainStoragePort.getUser(username)
                        .toEither(new UserFailure("user not found")));
    }

}
