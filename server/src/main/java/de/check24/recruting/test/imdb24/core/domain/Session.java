package de.check24.recruting.test.imdb24.core.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Session {
    public final Long created;
    public final String username;

    @JsonCreator
    public Session(@JsonProperty("created") final Long created, @JsonProperty("username") final String username) {
        this.created = created;
        this.username = username;
    }
}
