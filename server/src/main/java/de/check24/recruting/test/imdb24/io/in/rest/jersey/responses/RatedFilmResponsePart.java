package de.check24.recruting.test.imdb24.io.in.rest.jersey.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.check24.recruting.test.imdb24.core.domain.Rating;

public class RatedFilmResponsePart {
    @JsonProperty
    public Long filmId;
    @JsonProperty
    public Integer rating;

    @JsonCreator
    private RatedFilmResponsePart(@JsonProperty Long filmId,@JsonProperty Integer rating) {
        this.filmId = filmId;
        this.rating = rating;
    }

    public static RatedFilmResponsePart fromRatedFilm(Rating rating) {
        return new RatedFilmResponsePart(rating.filmId, rating.rating);
    }
}
