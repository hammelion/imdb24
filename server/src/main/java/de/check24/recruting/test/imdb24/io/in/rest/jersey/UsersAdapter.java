package de.check24.recruting.test.imdb24.io.in.rest.jersey;

import de.check24.recruting.test.imdb24.core.UserInteractor;
import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import de.check24.recruting.test.imdb24.io.in.rest.UsersPort;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.responses.UserResponse;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import static de.check24.recruting.test.imdb24.io.in.responses.BadResponse.badResponse;
import static de.check24.recruting.test.imdb24.util.Constants.SESSION_COOKIE_NAME;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

@Path("/rest/currentuser")
@Produces(MediaType.APPLICATION_JSON)
public class UsersAdapter implements UsersPort {
    private final UserInteractor userInteractor;
    @Context
    UriInfo uriInfo;

    @Inject
    public UsersAdapter(final UserInteractor userInteractor) {
        this.userInteractor = userInteractor;
    }

    @GET
    public Response current(@CookieParam(SESSION_COOKIE_NAME) @DefaultValue("") Cookie sessionId) {
        return SessionId.of(sessionId.getValue())
                .mapLeft(f -> badResponse(BAD_REQUEST, f.errors()))
                .flatMap(u -> userInteractor.getCurrentUser(u)
                        .mapLeft(f -> badResponse(BAD_REQUEST, f.reason)))
                .map(user -> Response.ok()
                        .entity(UserResponse.fromUser(user))
                        .links(Link.fromUri(uriInfo.getAbsolutePath())
                                .rel("self")
                                .type("GET")
                                .build())
                        .build())
                .fold(r -> r, r -> r);
    }
}