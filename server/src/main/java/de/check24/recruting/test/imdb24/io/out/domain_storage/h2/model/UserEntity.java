package de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model;

import com.google.common.collect.ImmutableList;
import de.check24.recruting.test.imdb24.core.domain.User;
import de.check24.recruting.test.imdb24.core.domain.validated.Username;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Entity(name = "User")
@Table(name = "user", indexes = {@Index(name = "username", columnList = "username", unique = true),
        @Index(name = "password", columnList = "password", unique = false)})
public class UserEntity {
    @Id
    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<RatingEntity> ratedFilms;

    public UserEntity(String username, String password, List<RatingEntity> ratedFilms) {
        this.username = username;
        this.password = password;
        this.ratedFilms = ratedFilms;
    }

    public UserEntity(String username) {
        this.username = username;
    }

    public UserEntity() {
    }

    public Optional<User> toUser() {
        return Username.of(getUsername())
                .toJavaOptional()
                .map(username -> new User(username, getRatedFilms().stream()
                        .map(RatingEntity::toRating)
                        .collect(collectingAndThen(toList(), ImmutableList::copyOf)))
                );
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public List<RatingEntity> getRatedFilms() {
        return ratedFilms;
    }
}
