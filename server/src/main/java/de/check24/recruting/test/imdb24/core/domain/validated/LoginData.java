package de.check24.recruting.test.imdb24.core.domain.validated;

import com.google.common.base.Strings;
import de.check24.recruting.test.imdb24.util.FailedValidation;
import de.check24.recruting.test.imdb24.util.ValidationError;
import io.vavr.control.Either;

import java.util.ArrayList;

import static com.google.common.hash.Hashing.sha256;
import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;
import static java.nio.charset.StandardCharsets.UTF_8;

public class LoginData {
    public final String username;
    public final String encodedPassword;

    private LoginData(final String username, final String password) {
        this.username = username;
        this.encodedPassword = sha256().hashString(password, UTF_8)
                .toString();
    }

    public static Either<FailedValidation<LoginData>, LoginData> of(final String username, final String password) {
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (Strings.isNullOrEmpty(username)) {
            errors.add(new ValidationError("username", "Is empty", username));
        }
        if (Strings.isNullOrEmpty(password) || password.length() < 8) {
            errors.add(new ValidationError("password", "Unsafe password"));
        }
        return errors.isEmpty() ? right(new LoginData(username, password)) : left(new FailedValidation<>(LoginData.class, errors));
    }
}
