package de.check24.recruting.test.imdb24.io.in.rest.jersey.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import de.check24.recruting.test.imdb24.core.domain.Film;

import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class FilmSearchResponse {
    @JsonProperty
    public final List<FilmSearchResponsePart> films;

    @JsonCreator
    private FilmSearchResponse(@JsonProperty List<FilmSearchResponsePart> films) {
        this.films = films;
    }

    public static FilmSearchResponse fromFilmList(List<Film> list) {
        return new FilmSearchResponse(list.stream()
                .map(FilmSearchResponsePart::fromFilm)
                .collect(collectingAndThen(toList(), ImmutableList::copyOf)));
    }
}
