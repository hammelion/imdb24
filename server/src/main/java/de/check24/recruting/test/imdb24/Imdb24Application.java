package de.check24.recruting.test.imdb24;

import de.check24.recruting.test.imdb24.io.in.auth.AuthFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;

/**
 * Challenge: Build a web application consisting of persistence layer, business logic, RESTful API, and a web frontend.
 * The application should be a mini-MVP of an IMDB-like application.
 * The backend should be based on Spring Boot and JPA; the database can be freely chosen.
 * The RESTful API should meet the typical norms of REST
 * Any additions to the data model for enhanced functionality are completely up to you!
 * Functionality, clean code and data model are more important than a refined UI/UX
 *
 * @see <a href=https://medium.com/@msmechatronics/hexagonal-architecture-in-java-5b21ebea849d>Hexagonal Architecture</a>
 */
@SpringBootApplication
public class Imdb24Application {

    public static void main(String[] args) {
        SpringApplication.run(Imdb24Application.class, args);
    }

    @Bean
    public FilterRegistrationBean<AuthFilter> authFilter() {
        FilterRegistrationBean<AuthFilter> registrationBean = new FilterRegistrationBean<>();

        registrationBean.setFilter(new AuthFilter());
        registrationBean.addUrlPatterns("/rest/*");

        return registrationBean;
    }
}
