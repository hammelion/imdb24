package de.check24.recruting.test.imdb24.io.in.rest.jersey;

import de.check24.recruting.test.imdb24.core.FilmInteractor;
import de.check24.recruting.test.imdb24.core.domain.FilmSearch;
import de.check24.recruting.test.imdb24.io.in.rest.FilmsPort;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.responses.FilmSearchResponse;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.*;

@Path("/rest/films")
@Produces(MediaType.APPLICATION_JSON)
public class FilmsAdapter implements FilmsPort {
    public static final String P_NAME = "name";
    public static final String P_PAGE = "page";
    public static final String P_SIZE = "size";
    private final FilmInteractor filmInteractor;
    @Context
    UriInfo uriInfo;

    @Inject
    public FilmsAdapter(final FilmInteractor filmInteractor) {
        this.filmInteractor = filmInteractor;
    }

    /**
     * Logged-in users can search for films by name, or see a list of all films.
     */
    @GET
    public Response search(@QueryParam(P_NAME) String name,
                           @QueryParam(P_PAGE) String limit,
                           @QueryParam(P_SIZE) String skip) {
        return Response.ok()
                .entity(FilmSearchResponse.fromFilmList(filmInteractor.search(FilmSearch.of(name, limit, skip))))
                .links(Link.fromUri(uriInfo.getAbsolutePath())
                        .rel("self")
                        .type("GET")
                        .build())
                .build();
    }
}