package de.check24.recruting.test.imdb24.io.in.auth.jersey;

import de.check24.recruting.test.imdb24.core.SecurityInteractor;
import de.check24.recruting.test.imdb24.core.domain.validated.LoginData;
import de.check24.recruting.test.imdb24.io.in.auth.LoginPort;
import org.springframework.core.env.Environment;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import static de.check24.recruting.test.imdb24.io.in.responses.BadResponse.badResponse;
import static de.check24.recruting.test.imdb24.util.Constants.SESSION_COOKIE_NAME;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

@Path("/auth/login")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
@Named
public class LoginAdapter implements LoginPort {
    private final SecurityInteractor securityInteractor;
    private String domain;
    private Integer timeout;

    @Inject
    public LoginAdapter(final SecurityInteractor securityInteractor, final Environment environment) {
        this.securityInteractor = securityInteractor;
        this.domain = environment.getProperty("imdb24.domain");
        this.timeout = Integer.valueOf(environment.getProperty("imdb24.encryption_session_storage.session_timeout_in_minutes"));
    }

    @POST
    public Response login(@FormParam("username") String username, @FormParam("password") String password) {
        return LoginData.of(username, password)
                .mapLeft(f -> badResponse(UNAUTHORIZED, f.errors()))
                .flatMap(loginData -> securityInteractor.login(loginData)
                        .mapLeft(f -> badResponse(UNAUTHORIZED, f.reason)))
                .map(sessionId -> Response.status(ACCEPTED)
                        .cookie(new NewCookie(SESSION_COOKIE_NAME, sessionId.sessionId, "/", domain, "", timeout * 60, false))
                        .build())
                .fold(r -> r, r -> r);
    }
}
