package de.check24.recruting.test.imdb24.io.in.rest;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;

public interface UsersPort {
    Response current(Cookie sessionId);
}
