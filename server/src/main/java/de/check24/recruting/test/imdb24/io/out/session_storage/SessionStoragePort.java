package de.check24.recruting.test.imdb24.io.out.session_storage;

import de.check24.recruting.test.imdb24.core.domain.Session;
import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import de.check24.recruting.test.imdb24.core.domain.User;
import de.check24.recruting.test.imdb24.io.out.session_storage.responses.FailedSessionRequest;
import io.vavr.control.Either;

public interface SessionStoragePort {
    Either<FailedSessionRequest, SessionId> createSession(final User user);
    Either<FailedSessionRequest, Session> getSession(final SessionId sessionId);
}
