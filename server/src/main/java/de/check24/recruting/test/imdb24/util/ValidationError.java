package de.check24.recruting.test.imdb24.util;

import java.util.Optional;

public class ValidationError {
    public final String fieldName;
    public final String message;
    public final Optional<String> value;

    public ValidationError(final String fieldName, final String message, final String value) {
        this.fieldName = fieldName;
        this.message = message;
        this.value = Optional.ofNullable(value);
    }

    public ValidationError(String fieldName, String message) {
        this(fieldName, message, null);
    }

    @Override
    public String toString() {
        return "fieldName='" + fieldName + '\'' +
                ", message='" + message + '\'' +
                ", value=" + value.orElse("none");
    }
}
