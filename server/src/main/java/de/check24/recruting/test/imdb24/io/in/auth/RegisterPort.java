package de.check24.recruting.test.imdb24.io.in.auth;

import javax.ws.rs.core.Response;

public interface RegisterPort {
    Response register(String username, String password);
}
