package de.check24.recruting.test.imdb24.core.domain.validated;

import com.google.common.base.Strings;
import de.check24.recruting.test.imdb24.util.FailedValidation;
import de.check24.recruting.test.imdb24.util.ValidationError;
import io.vavr.control.Either;

import java.util.ArrayList;

import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;

public class Username {
    public final String value;

    public Username(final String value) {
        this.value = value;
    }

    public static Either<FailedValidation<Username>, Username> of(final String username) {
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (Strings.isNullOrEmpty(username)) {
            errors.add(new ValidationError("username", "Is empty", username));
        }

        return errors.isEmpty() ? right(new Username(username)) : left(new FailedValidation<>(Username.class, errors));
    }
}
