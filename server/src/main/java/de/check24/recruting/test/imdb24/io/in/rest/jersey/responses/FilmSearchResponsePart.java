package de.check24.recruting.test.imdb24.io.in.rest.jersey.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.check24.recruting.test.imdb24.core.domain.Film;

public class FilmSearchResponsePart {
    @JsonProperty
    public final Long id;
    @JsonProperty
    public final String name;
    @JsonProperty
    public final Integer avgRating;

    @JsonCreator
    private FilmSearchResponsePart(@JsonProperty final Long id, @JsonProperty final String name, @JsonProperty final Integer avgRating) {
        this.id = id;
        this.name = name;
        this.avgRating = avgRating;
    }

    public static FilmSearchResponsePart fromFilm(final Film film) {
        return new FilmSearchResponsePart(film.id, film.name, film.avgRating);
    }
}
