package de.check24.recruting.test.imdb24.io.in.rest.jersey.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

public class UserResponse {
    @JsonProperty
    public final String username;
    @JsonProperty
    public final List<RatedFilmResponsePart> ratedFilms;


    @JsonCreator
    private UserResponse(@JsonProperty final String username, @JsonProperty List<RatedFilmResponsePart> ratedFilms) {
        this.username = username;
        this.ratedFilms = ratedFilms;
    }

    public static UserResponse fromUser(de.check24.recruting.test.imdb24.core.domain.User user) {
        return new UserResponse(user.username.value, user.ratings.stream()
                .map(RatedFilmResponsePart::fromRatedFilm)
                .collect(collectingAndThen(toList(), ImmutableList::copyOf)));
    }
}
