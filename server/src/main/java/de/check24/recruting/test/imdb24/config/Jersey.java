package de.check24.recruting.test.imdb24.config;

import de.check24.recruting.test.imdb24.io.in.auth.jersey.LoginAdapter;
import de.check24.recruting.test.imdb24.io.in.auth.jersey.RegisterAdapter;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.FilmsAdapter;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.RatingsAdapter;
import de.check24.recruting.test.imdb24.io.in.rest.jersey.UsersAdapter;
import org.glassfish.jersey.server.ResourceConfig;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class Jersey extends ResourceConfig {
    @Inject
    public Jersey() {
        register(LoginAdapter.class)
                .register(UsersAdapter.class)
                .register(FilmsAdapter.class)
                .register(RatingsAdapter.class)
                .register(RegisterAdapter.class);
    }
}