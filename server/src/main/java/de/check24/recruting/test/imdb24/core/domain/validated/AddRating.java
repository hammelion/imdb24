package de.check24.recruting.test.imdb24.core.domain.validated;

import de.check24.recruting.test.imdb24.util.FailedValidation;
import de.check24.recruting.test.imdb24.util.ValidationError;
import io.vavr.control.Either;
import io.vavr.control.Try;

import java.util.ArrayList;
import java.util.Optional;

import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;

public class AddRating {
    public final Long filmId;
    public final Integer rating;
    public final Username username;

    public AddRating(Long filmId, Integer rating, Username username) {
        this.filmId = filmId;
        this.rating = rating;
        this.username = username;
    }

    public static Either<FailedValidation<AddRating>, AddRating> of(final String filmId, final String rating, final Username username) {
        ArrayList<ValidationError> errors = new ArrayList<>();
        Optional<Long> oFilmId = Optional.ofNullable(filmId)
                .flatMap(v -> Try.of(() -> Long.valueOf(v))
                        .toJavaOptional());
        if (!oFilmId.isPresent()) {
            errors.add(new ValidationError("film_id", "Is invalid", filmId));
        }
        Optional<Integer> oRating = Optional.ofNullable(rating)
                .flatMap(v -> Try.of(() -> Integer.valueOf(v))
                        .toJavaOptional())
                .filter(v -> v >= 1 && v <= 5);
        if (!oRating.isPresent()) {
            errors.add(new ValidationError("rating", "Expected number from 1 to 5", rating));
        }
        return errors.isEmpty() ? right(new AddRating(oFilmId.get(), oRating.get(), username)) : left(new FailedValidation<>(AddRating.class, errors));
    }
}
