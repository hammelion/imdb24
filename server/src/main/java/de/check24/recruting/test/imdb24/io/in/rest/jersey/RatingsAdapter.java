package de.check24.recruting.test.imdb24.io.in.rest.jersey;

import de.check24.recruting.test.imdb24.core.FilmInteractor;
import de.check24.recruting.test.imdb24.core.UserInteractor;
import de.check24.recruting.test.imdb24.core.domain.validated.AddRating;
import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import de.check24.recruting.test.imdb24.io.in.rest.RatingsPort;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;

import static de.check24.recruting.test.imdb24.io.in.responses.BadResponse.badResponse;
import static de.check24.recruting.test.imdb24.util.Constants.SESSION_COOKIE_NAME;
import static javax.ws.rs.core.Response.Status.*;

@Path("/rest/ratings")
@Produces(MediaType.APPLICATION_JSON)
public class RatingsAdapter implements RatingsPort {
    public static final String P_FILM = "film_id";
    public static final String P_RATING = "rating";
    private final FilmInteractor filmInteractor;
    private final UserInteractor userInteractor;

    @Context
    UriInfo uriInfo;

    @Inject
    public RatingsAdapter(final FilmInteractor filmInteractor, final UserInteractor userInteractor) {
        this.filmInteractor = filmInteractor;
        this.userInteractor = userInteractor;
    }

    /**
     * A film can also be rated by a logged-in user;
     * A Film can be rated on a scale of 1-5 by users.
     */
    @POST
    public Response rate(@QueryParam(P_FILM) String filmId,
                         @QueryParam(P_RATING) String rating,
                         @CookieParam(SESSION_COOKIE_NAME) @DefaultValue("") Cookie sessionId) {
        return SessionId.of(sessionId.getValue())
                .mapLeft(f -> badResponse(BAD_REQUEST, f.errors()))
                .flatMap(u -> userInteractor.getCurrentUser(u)
                        .mapLeft(f -> badResponse(BAD_REQUEST, f.reason)))
                .flatMap(user -> AddRating.of(filmId, rating, user.username)
                        .mapLeft(f -> badResponse(BAD_REQUEST, f.errors())))
                .flatMap(addRating -> filmInteractor.rate(addRating)
                        .toEither(badResponse(INTERNAL_SERVER_ERROR, "Could not save rating")))
                .map(ratingId -> Response.status(CREATED)
                        .links(Link.fromUri(uriInfo.getAbsolutePath())
                                .rel("self")
                                .type("POST")
                                .build())
                        .build())
                .fold(r -> r, r -> r);
    }
}