package de.check24.recruting.test.imdb24.core.domain.failures;

public class UserFailure {
    public final String reason;

    public UserFailure(final String reason) {
        this.reason = reason;
    }
}
