package de.check24.recruting.test.imdb24.core.domain;

import java.util.List;

public class Film {
    public final Long id;
    public final String name;
    public final Integer avgRating;
    public final List<Rating> ratings;

    public Film(final Long id, final String name, final Integer avgRating, final List<Rating> ratings) {
        this.id = id;
        this.name = name;
        this.avgRating = avgRating;
        this.ratings = ratings;
    }
}
