package de.check24.recruting.test.imdb24.core;

import de.check24.recruting.test.imdb24.core.domain.validated.AddRating;
import de.check24.recruting.test.imdb24.core.domain.Film;
import de.check24.recruting.test.imdb24.core.domain.FilmSearch;
import de.check24.recruting.test.imdb24.io.out.domain_storage.DomainStoragePort;
import io.vavr.control.Option;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class FilmInteractor {
    private final DomainStoragePort domainStoragePort;

    @Inject
    public FilmInteractor(final DomainStoragePort domainStoragePort) {
        this.domainStoragePort = domainStoragePort;
    }

    public List<Film> search(final FilmSearch filmSearch) {
        return domainStoragePort.searchFilms(filmSearch);
    }

    public Option<Long> rate(final AddRating addRating) {
        return domainStoragePort.addRating(addRating);
    }

}
