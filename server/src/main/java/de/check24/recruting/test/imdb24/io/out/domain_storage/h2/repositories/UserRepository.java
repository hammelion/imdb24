package de.check24.recruting.test.imdb24.io.out.domain_storage.h2.repositories;

import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, String> {
}
