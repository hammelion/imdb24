package de.check24.recruting.test.imdb24.util;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.unmodifiableList;

public class FailedValidation<T> {
    public final Class<T> entityType;
    public final List<ValidationError> errors;

    public FailedValidation(final Class<T> entityType, final List<ValidationError> errors) {
        this.entityType = entityType;
        this.errors = unmodifiableList(errors);
    }

    public String errors() {
        return errors.stream()
                .map(ValidationError::toString)
                .collect(Collectors.joining("\n"));
    }
}
