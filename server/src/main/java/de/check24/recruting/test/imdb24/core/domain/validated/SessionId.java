package de.check24.recruting.test.imdb24.core.domain.validated;

import com.google.common.base.Strings;
import de.check24.recruting.test.imdb24.util.FailedValidation;
import de.check24.recruting.test.imdb24.util.ValidationError;
import io.vavr.control.Either;

import static io.vavr.control.Either.left;
import static io.vavr.control.Either.right;
import static java.util.Collections.singletonList;

public class SessionId {
    public final String sessionId;

    public SessionId(final String sessionId) {
        this.sessionId = sessionId;
    }

    public static Either<FailedValidation<SessionId>, SessionId> of(final String sessionId) {
        if (Strings.isNullOrEmpty(sessionId)) {
            return left(new FailedValidation(SessionId.class, singletonList(new ValidationError("sessionId", "Wrong session id"))));
        }
        return right(new SessionId(sessionId));
    }
}
