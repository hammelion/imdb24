package de.check24.recruting.test.imdb24.io.out.domain_storage.h2.repositories;

import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model.FilmEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FilmRepository extends JpaRepository<FilmEntity, Long> {
    List<FilmEntity> findByNameContaining(String name, Pageable pageable);
}
