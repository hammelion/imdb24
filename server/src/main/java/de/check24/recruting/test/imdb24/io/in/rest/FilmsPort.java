package de.check24.recruting.test.imdb24.io.in.rest;

import javax.ws.rs.core.Response;

public interface FilmsPort {
    Response search(String name, String limit, String skip);
}
