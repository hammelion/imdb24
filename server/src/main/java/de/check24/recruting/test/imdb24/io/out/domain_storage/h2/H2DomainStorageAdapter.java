package de.check24.recruting.test.imdb24.io.out.domain_storage.h2;

import com.google.common.collect.ImmutableList;
import de.check24.recruting.test.imdb24.core.domain.Film;
import de.check24.recruting.test.imdb24.core.domain.FilmSearch;
import de.check24.recruting.test.imdb24.core.domain.User;
import de.check24.recruting.test.imdb24.core.domain.validated.AddRating;
import de.check24.recruting.test.imdb24.core.domain.validated.LoginData;
import de.check24.recruting.test.imdb24.core.domain.validated.Username;
import de.check24.recruting.test.imdb24.io.out.domain_storage.DomainStoragePort;
import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model.FilmEntity;
import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model.RatingEntity;
import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model.UserEntity;
import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.repositories.FilmRepository;
import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.repositories.RatingRepository;
import de.check24.recruting.test.imdb24.io.out.domain_storage.h2.repositories.UserRepository;
import io.vavr.control.Option;
import io.vavr.control.Try;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.PageRequest;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Named
public class H2DomainStorageAdapter implements DomainStoragePort {
    private static Logger LOG = LoggerFactory.getLogger(H2DomainStorageAdapter.class);

    private final UserRepository userRepository;
    private final FilmRepository filmRepository;
    private final RatingRepository ratingRepository;

    @Inject
    public H2DomainStorageAdapter(final UserRepository userRepository, final FilmRepository filmRepository, RatingRepository ratingRepository) {
        this.userRepository = userRepository;
        this.filmRepository = filmRepository;
        this.ratingRepository = ratingRepository;
    }

    @Override
    public Option<User> authenticateUser(LoginData loginData) {
        return Option.ofOptional(Try.of(() ->
                userRepository.findOne(Example.of(new UserEntity(loginData.username, loginData.encodedPassword, null))
                ))
                .onFailure(e -> LOG.error(e.getMessage(), e))
                .toJavaOptional()
                .flatMap(o -> o)
                .flatMap(UserEntity::toUser));
    }

    @Override
    public Option<User> createUser(LoginData loginData) {
        return Option.ofOptional(Try.of(() ->
                userRepository.save(new UserEntity(loginData.username, loginData.encodedPassword, new ArrayList<>())))
                .onFailure(e -> LOG.error(e.getMessage(), e))
                .toJavaOptional()
                .flatMap(UserEntity::toUser));
    }

    @Override
    public Option<User> getUser(Username username) {
        return Option.ofOptional(Try.of(() ->
                userRepository.findById(username.value))
                .onFailure(e -> LOG.error(e.getMessage(), e))
                .toJavaOptional()
                .flatMap(o -> o)
                .flatMap(UserEntity::toUser));
    }

    @Override
    public List<Film> searchFilms(FilmSearch filmSearch) {
        return Try.of(() ->
                filmRepository.findByNameContaining(
                        filmSearch.name.orElse(""),
                        PageRequest.of(filmSearch.page.orElse(0), filmSearch.size.orElse(10))))
                .onFailure(e -> LOG.error(e.getMessage(), e))
                .map(list -> list.stream()
                        .map(FilmEntity::toFilm)
                        .collect(collectingAndThen(toList(), ImmutableList::copyOf)))
                .getOrElse(ImmutableList::of);
    }

    @Override
    public Option<Long> addRating(AddRating addRating) {
        return Try.of(() -> {
                    ratingRepository.findOne(Example.of(
                            new RatingEntity(
                                    new UserEntity(addRating.username.value),
                                    new FilmEntity(addRating.filmId), null)))
                            .ifPresent(e -> ratingRepository.deleteById(e.getId()));
                    return ratingRepository.save(new RatingEntity(
                            new UserEntity(addRating.username.value),
                            new FilmEntity(addRating.filmId),
                            addRating.rating));
                }
        )
                .onFailure(e -> LOG.error(e.getMessage(), e))
                .map(RatingEntity::getId)
                .toOption();
    }
}
