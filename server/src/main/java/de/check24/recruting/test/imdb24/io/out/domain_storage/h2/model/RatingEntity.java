package de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model;

import de.check24.recruting.test.imdb24.core.domain.Rating;

import javax.persistence.*;

@Entity(name = "Rating")
@Table(name = "rating")
public class RatingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user")
    private UserEntity user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="film")
    private FilmEntity film;
    @Column(name = "rating", nullable = false)
    private Integer rating;

    public RatingEntity(UserEntity user, FilmEntity film, Integer rating) {
        this.user = user;
        this.film = film;
        this.rating = rating;
    }

    public RatingEntity() {
    }

    public Rating toRating() {
        return new Rating(getUser().getUsername(), getFilm().getId(), getRating());
    }

    public Long getId() {
        return id;
    }

    public UserEntity getUser() {
        return user;
    }

    public FilmEntity getFilm() {
        return film;
    }

    public Integer getRating() {
        return rating;
    }
}
