package de.check24.recruting.test.imdb24.core.domain;

public class Rating {
    public final String username;
    public final Long filmId;
    public final Integer rating;

    public Rating(final String username, final Long filmId, final Integer rating) {
        this.username = username;
        this.filmId = filmId;
        this.rating = rating;
    }
}
