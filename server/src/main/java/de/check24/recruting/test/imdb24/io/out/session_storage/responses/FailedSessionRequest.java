package de.check24.recruting.test.imdb24.io.out.session_storage.responses;

public class FailedSessionRequest {
    public final Throwable e;

    public FailedSessionRequest(final Throwable e) {
        this.e = e;
    }
}
