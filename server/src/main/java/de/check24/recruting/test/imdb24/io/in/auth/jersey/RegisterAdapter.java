package de.check24.recruting.test.imdb24.io.in.auth.jersey;

import de.check24.recruting.test.imdb24.core.SecurityInteractor;
import de.check24.recruting.test.imdb24.core.domain.validated.LoginData;
import de.check24.recruting.test.imdb24.io.in.auth.RegisterPort;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static de.check24.recruting.test.imdb24.io.in.responses.BadResponse.badResponse;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;

@Path("/auth/register")
@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
@Produces(MediaType.APPLICATION_JSON)
public class RegisterAdapter implements RegisterPort {
    private final SecurityInteractor securityInteractor;

    @Inject
    public RegisterAdapter(final SecurityInteractor securityInteractor) {
        this.securityInteractor = securityInteractor;
    }

    /**
     * A User can register themselves in the application (with username and password).
     */
    @POST
    // TODO Captcha
    public Response register(@FormParam("username") String username, @FormParam("password") String password) {
        return LoginData.of(username, password)
                .mapLeft(f -> badResponse(BAD_REQUEST, f.errors()))
                .flatMap(loginData -> securityInteractor.register(loginData)
                        .mapLeft(f -> badResponse(BAD_REQUEST, f.reason)))
                .map(user -> Response.status(CREATED)
                        .build())
                .fold(r -> r, r -> r);
    }
}
