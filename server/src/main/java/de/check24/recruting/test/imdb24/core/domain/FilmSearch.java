package de.check24.recruting.test.imdb24.core.domain;

import io.vavr.control.Try;

import java.util.Optional;

public class FilmSearch {
    public final Optional<String> name;
    public final Optional<Integer> page;
    public final Optional<Integer> size;

    private FilmSearch(final Optional<String> name, final Optional<Integer> page, final Optional<Integer> size) {
        this.name = name;
        this.page = page;
        this.size = size;
    }

    public static FilmSearch of(final String name, final String page, final String size) {
        return new FilmSearch(
                Optional.ofNullable(name),
                Optional.ofNullable(page)
                        .flatMap(v -> Try.of(() -> Integer.valueOf(v))
                                .toJavaOptional()),
                Optional.ofNullable(size)
                        .flatMap(v -> Try.of(() -> Integer.valueOf(v))
                                .toJavaOptional()));
    }
}
