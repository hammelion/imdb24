package de.check24.recruting.test.imdb24.core.domain;

import de.check24.recruting.test.imdb24.core.domain.validated.Username;

import java.util.List;

public class User {
    public final Username username;
    public final List<Rating> ratings;


    public User(final Username username, List<Rating> ratings) {
        this.username = username;
        this.ratings = ratings;
    }
}
