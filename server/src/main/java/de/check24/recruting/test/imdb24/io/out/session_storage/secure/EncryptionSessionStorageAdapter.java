package de.check24.recruting.test.imdb24.io.out.session_storage.secure;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.check24.recruting.test.imdb24.core.domain.Session;
import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import de.check24.recruting.test.imdb24.core.domain.User;
import de.check24.recruting.test.imdb24.io.out.session_storage.responses.FailedSessionRequest;
import de.check24.recruting.test.imdb24.io.out.session_storage.SessionStoragePort;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.springframework.beans.factory.annotation.Value;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.inject.Named;
import java.time.Duration;
import java.time.Instant;
import java.util.Base64;

@Named
public class EncryptionSessionStorageAdapter implements SessionStoragePort {
    @Value("${imdb24.encryption_session_storage.key}")
    private String key;
    @Value("${imdb24.encryption_session_storage.session_timeout_in_minutes}")
    private Integer timeout;

    @Override
    public Either<FailedSessionRequest, SessionId> createSession(User user) {
        return Try.of(() -> {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec aesKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, aesKey, new IvParameterSpec(new byte[16]));

            return new SessionId(Base64.getEncoder()
                    .encodeToString(
                            cipher.doFinal(
                                    new ObjectMapper()
                                            .writeValueAsString(new Session(Instant.now()
                                                    .toEpochMilli(), user.username.value))
                                            .getBytes())));
        })
                .toEither()
                .mapLeft(FailedSessionRequest::new);
    }

    @Override
    public Either<FailedSessionRequest, Session> getSession(SessionId sessionId) {
        return Try.of(() -> {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            SecretKeySpec aesKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.DECRYPT_MODE, aesKey, new IvParameterSpec(new byte[16]));
            Session session = new ObjectMapper().readValue(new String(
                    cipher.doFinal(
                            Base64.getDecoder()
                                    .decode(sessionId.sessionId.getBytes())
                    )), Session.class);
            if (Duration.between(Instant.ofEpochMilli(session.created), Instant.now())
                    .toMinutes() < timeout) {
                return session;
            }
            throw new Exception("Session expired");
        })
                .toEither()
                .mapLeft(FailedSessionRequest::new);
    }
}
