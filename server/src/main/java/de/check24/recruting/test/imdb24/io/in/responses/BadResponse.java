package de.check24.recruting.test.imdb24.io.in.responses;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.ws.rs.core.Response;

public class BadResponse {
    public final String error;

    @JsonCreator
    public BadResponse(@JsonProperty final String error) {
        this.error = error;
    }

    public static Response badResponse(final Response.Status status, final String error) {
        return Response.status(status)
                .entity(new BadResponse(error))
                .build();
    }
}
