package de.check24.recruting.test.imdb24.io.in.auth;

import de.check24.recruting.test.imdb24.core.SecurityInteractor;
import de.check24.recruting.test.imdb24.core.domain.Session;
import de.check24.recruting.test.imdb24.core.domain.validated.SessionId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import static de.check24.recruting.test.imdb24.util.Constants.SESSION_COOKIE_NAME;

@Order(2)
public class AuthFilter implements Filter {
    private static Logger LOG = LoggerFactory.getLogger(AuthFilter.class);

    private SecurityInteractor securityInteractor;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        securityInteractor = WebApplicationContextUtils.
                getRequiredWebApplicationContext(filterConfig.getServletContext())
                .getBean(SecurityInteractor.class);
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        Optional<Session> authenticated = Optional.ofNullable(req.getCookies())
                .flatMap(cs -> Arrays.stream(cs)
                        .filter(c -> SESSION_COOKIE_NAME.equals(c.getName()))
                        .findFirst()
                        .map(Cookie::getValue)
                        .map(SessionId::of)
                        .flatMap(e -> e.peekLeft(f -> LOG.warn(f.errors()))
                                .toJavaOptional())
                        .map(securityInteractor::authenticate)
                        .flatMap(e -> e.peekLeft(f -> LOG.error(f.reason))
                                .toJavaOptional()));
        if (!(authenticated.isPresent())) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        chain.doFilter(req, res);
    }
}
