package de.check24.recruting.test.imdb24.io.out.domain_storage.h2.model;

import com.google.common.collect.ImmutableList;
import de.check24.recruting.test.imdb24.core.domain.Film;

import javax.persistence.*;
import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Entity(name = "Film")
@Table(name = "film", indexes = {@Index(name = "id", columnList = "id", unique = true),
        @Index(name = "name", columnList = "name", unique = false)})
public class FilmEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "film", fetch = FetchType.EAGER)
    private List<RatingEntity> ratings;

    public FilmEntity(Long id, String name, List<RatingEntity> ratings) {
        this.id = id;
        this.name = name;
        this.ratings = ratings;
    }

    public FilmEntity(Long id) {
        this.id = id;
    }

    public FilmEntity() {
    }

    public Film toFilm() {
        return new Film(getId(), getName(), getRatings().stream()
                .map(RatingEntity::getRating)
                .reduce(Integer::sum)
                .map(sum -> sum / getRatings().size())
                .orElse(0),
                getRatings().stream()
                        .map(RatingEntity::toRating)
                        .collect(collectingAndThen(toList(), ImmutableList::copyOf)));
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<RatingEntity> getRatings() {
        return ratings;
    }
}
